from unicodedata import name
from django.urls import path

from .views import *

urlpatterns = [
    #serve index.html
    path("", index, name="index"),

    #GET books
    path("books", BookList.as_view(), name="book-list"),

    #specific book
    path('book/<int:id>/', SpecificBook.as_view(), name="book"),

]