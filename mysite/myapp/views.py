from asyncio.windows_events import NULL
from http.client import HTTPResponse
from msilib.schema import ServiceInstall
from django.shortcuts import render
from django.http import HttpResponse, JsonResponse, Http404
from .models import Book
from .serializers import BookSerializer

from rest_framework.views import APIView
from rest_framework import status

# Create your views here.

def index(request):
    return HttpResponse("Hello world ! You are at the myapp index")

class BookList(APIView):
    """
    List all books, or creat a new book
    """

    def get(self, request, format=None):
        books = Book.objects.all()
        serializer = BookSerializer(books, many=True)
        return JsonResponse(serializer.data, safe=False)

    def post(self, request, format=None):
        serializer = BookSerializer(data = request.data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data, safe=False, status=status.HTTP_201_CREATED)
        return JsonResponse(serializer.errors, safe=False, status=status.HTTP_400_BAD_REQUEST)

#faire une seule classe Book qui contient les trois fonctions : get, delete et put
class SpecificBook(APIView):
    def get_object(self, id):
        try:
            return Book.objects.get(id=id)
        except Book.DoesNotExist:
            raise Http404

    def get(self, request, id, format=None):
        book = self.get_object(id)
        serializer = BookSerializer(book)
        return JsonResponse(serializer.data, safe=False)

    def put(self, request, id, format=None):
        book = self.get_object(id)
        serializer = BookSerializer(book, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data, safe=False)
        return JsonResponse(serializer.errors, safe=False, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, id, format=None):
        book = self.get_object(id)
        book.delete()
        return JsonResponse({}, safe=False, status=status.HTTP_204_NO_CONTENT)